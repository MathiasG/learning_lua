app = {
	params = function(args)
		local params = {}
		(function()
			for k, arg in pairs(args) do
				if k < 0 then return end
				local split = arg:sub(3):split("=")
				if #split ~= 2 then return end
				params[split[1]] = split[2]
			end
		end)()
		return params
	end
}