require "libs/luaobject"

rect = object:prototype {
	position = { x = 0, y = 0 },
	width = 0,
	height = 0,
}

function rect:intersects(other)
	return self.position.x >= other.position.x and self.position.x <= other.position.x + other.width
		and self.position.y + self.height >= other.position.y and self.position.y <= other.position.y + other.height
end

table.setreadonly(rect)