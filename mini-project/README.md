# Learning Lua: mini-project

## Sources for ideas *(not only applicable to Lua)*

- **I got my ideas from:** <https://www.reddit.com/r/lua/comments/3swmhr/lua_program_ideas/>
- **Another great source for ideas:** <https://www.reddit.com/r/lua/comments/3swmhr/lua_program_ideas/>

## Project description

This project is a love2D shooter (game). You are a submarine with as only goal surviving by shooting your enemies before they get to you. I will loosely base this project on a tutorial *(<https://dev.to/jeansberg/make-a-shooter-in-lualove2d---part-3>)* as my intent is not to get familiar with love2D itself. Eventually the game should look about the same (and probably be coded the same way) as the one in the tutorial.

## Controls

Controls are:

- **Up**: \<up-arrow>
- **Down**: \<down-arrow>
- **Left**: \<left-arrow>
- **Right**: \<right-arrow>
- **Torpedo 1**: \<1>
- **Torpedo 2**: \<2>
- **Torpedo 3**: \<3>
- **Torpedo 4**: \<4>
- **Restart game**: \<escape>
- **Debug**: \<rctrl> (needs CLI)
