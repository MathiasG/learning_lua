require "libs/luaobject"
require "libs/application"
require "config"
require "subby"

-- #region Properties

local screen = {
	width = love.graphics.getWidth(),
	height = love.graphics.getHeight(),
	showGameOver = false,
}
local audio = {}

-- #endregion

local updateSounds = function()
	if subby.event.explosion and not audio.explosion:isPlaying() then
		subby.event.explosion = false
		if subby.sounds.effects then
			audio.explosion:stop()
			audio.explosion:play()
		end
	end
	if subby.event.shoot then
		subby.event.shoot = false
		if subby.sounds.effects then
			audio.shoot:stop()
			audio.shoot:play()
		end
	end
	if subby.state.isStopped then
		subby.state.isStopped = false
		screen.showGameOver = true
		if subby.sounds.effects then
			audio.lightening:stop()
			audio.lightening:play()
		end
	end
	if subby.sounds.music and not audio.mercury:isPlaying() then
		audio.mercury:stop()
		audio.mercury:play()
	end
end

love.load = function(args)
	local params = app.params(args)
	-- love
	love.window.setTitle("Subby the submarine")
	love.window.setIcon(love.image.newImageData(config.submarine.sprite))
	audio = {
		explosion = love.audio.newSource(config.audio.explosion, "static"),
		lightening = love.audio.newSource(config.audio.lightening, "static"),
		mercury = love.audio.newSource(config.audio.mercury, "static"),
		shoot = love.audio.newSource(config.audio.shoot, "static")
	}
	-- subby
	if params.silent == "true" then table.join(params, { music = "false", effects = "false" }) end
	table.join(subby.sounds, { music = params.music ~= "false", effects = params.effects ~= "false" })
	subby:start()
end

love.draw = function()
	local img = love.graphics.newImage
	local draw = love.graphics.draw
	-- background
	love.graphics.setColor(.1, .5, .8)
	love.graphics.rectangle("fill", 0, 0, screen.width, screen.height)
	love.graphics.setColor(1, 1, 1)
	draw(img(config.ground.sprite), 0, screen.height - config.ground.height, 0, 2, 2)
	-- submarine
	draw(img(subby.submarine.sprite), subby.submarine.position.x, subby.submarine.position.y, 0, 2, 2)
	-- torpedoes & enemies
	from(subby.torpedoBay.torpedoes)
		:foreach(function(torpedo) draw(img(torpedo.sprite), torpedo.position.x, torpedo.position.y) end)
	from(subby.enemies)
		:foreach(function(enemy) draw(img(enemy.sprite), enemy.position.x, enemy.position.y, 0, 2, 2) end)
	-- info
	love.graphics.print("Hits/Misses/Passed by:\t"..subby.score.hits.."/"..subby.score.misses.."/"..subby.score.passes)
	local msg = "Top score: "..subby.score.topScore.."  Score: "..subby.score.value
	love.graphics.print(msg, screen.width - #msg * 7, 0)
	-- overlay
	if screen.showGameOver then draw(img(config.gameOver), 0, 0, 0, 2, 2) end
end

love.update = function(dTime)
	updateSounds()
	-- gamestate
	if love.keyboard.isDown("escape") then subby:start() end
	if subby.state.isPaused then return end
	screen.showGameOver = false
	subby:update(screen, {
		down = love.keyboard.isDown("down"),
		up = love.keyboard.isDown("up"),
		left = love.keyboard.isDown("left"),
		right = love.keyboard.isDown("right")
	}, table.new(config.torpedoTubes, function(i) return love.keyboard.isDown(i) end), dTime)
	subby:updateScore()
end

love.focus = function(focus) subby.state.isPaused = not focus end

function love.keypressed(key, u)
	--Debug
	if key == "rctrl" then --set to whatever key you want to use
	   debug.debug()
	end
 end