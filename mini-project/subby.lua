require "entities/submarine"
require "entities/enemies"

subby = {
	state = { isPaused = false, isStopped = false },
	sounds = { music = true, effects = true },
	event = { explosion = false, shoot = false },
	score = { topScore = 0 },
	_ = {}
}

function subby:start()
	if self.score.value and self.score.topScore < self.score.value then self.score.topScore = self.score.value end
	table.join(self, {
		init = true,
		torpedoBay = {
			tubes = table.new(config.torpedoTubes, function() return 0 end),
			torpedoes = {},
		},
		submarine = config.submarine:prototype {
			position = { x = 0, y = 0 },
			movement = {}
		},
		enemies = {},
		spawnTimer = config.spawnTimer:prototype { current = 0 },
		score = table.join(self.score, { hits = 0, misses =  0, passes = 0, value = 0, time = 0 }),
		state = table.join(self.state, { isPaused = false })
	})
end

function subby:update(canvas, moveRequest, torpedoRequest, dTime)
	if self.state.isPaused then return end
	self._ = {
		canvas = canvas,
		request = {
			move = moveRequest,
			torpedo = torpedoRequest,
		},
		dTime = dTime
	}
	if self.init then
		self.init = false
		self.submarine.position.y = (self._.canvas.height - self.submarine.height) / 2
	end
	self.score.time = self.score.time + self._.dTime
	submarine:update()
	enemies:update()
end

function subby:updateScore(multiplier)
	multiplier = table.join({ time = 2, hits = 5, passes = -5, misses = -0.5, value = 10 }, multiplier)
	self.score.value = math.floor(
		self.score.time * multiplier.time +
		self.score.hits * multiplier.hits +
		self.score.passes * multiplier.passes +
		self.score.misses * multiplier.misses
	) * multiplier.value
end