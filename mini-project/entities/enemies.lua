local updateMovement = function(enemies)

	local updateEnemy = function(enemy, i)

		local updateEnemyMovement = function()

			local move = function() enemy.position.x = enemy.position.x - enemy.speed * subby._.dTime end

			local chase = function()
				local speed = {
					x = math.cos(math.rad(enemy.attackAngle)) * enemy.speed,
					y = math.sin(math.rad(enemy.attackAngle)) * enemy.speed,
				}
				if enemy.position.y - subby.submarine.position.y > 10 then
					enemy.position = {
						x = enemy.position.x - speed.x * subby._.dTime,
						y = enemy.position.y - speed.y * subby._.dTime,
					}
				elseif enemy.position.y - subby.submarine.position.y < -10 then
					enemy.position = {
						x = enemy.position.x - speed.x * subby._.dTime,
						y = enemy.position.y + speed.y * subby._.dTime,
					}
				else move() end
			end

			local charge = function()
				local distance = {
					x = math.abs(enemy.position.x - subby.submarine.position.x),
					y = math.abs(enemy.position.y - subby.submarine.position.y),
				}
				distance = math.sqrt(distance.x ^ 2 + distance.y ^ 2)
				if distance < subby.submarine.position.x + enemy.chargeDistance then
					local chargeSpeed = enemy.speed * math.sqrt(2)
					enemy.speed = chargeSpeed > enemy.chargeSpeed and enemy.chargeSpeed or chargeSpeed
					move()
				else chase() end
			end

			if subby.submarine.position.x > enemy.position.x + enemy.width then move()
			elseif enemy.canCharge then charge()
			else chase() end
		end

		local deleteEnemy = function()
			-- update collisions
			if enemy:collides(subby.submarine) and not subby.submarine.ignored then
				subby.state.isStopped = true
				subby.state.isPaused = true
			end
			from(subby.torpedoBay.torpedoes):each(function(torpedo, ti)
				if not torpedo:collides(enemy) then return end
				table.remove(subby.torpedoBay.torpedoes, ti)
				table.remove(subby.enemies, i)
				subby.event.explosion = true
				subby.score.hits = subby.score.hits + 1
			end)
			-- remove when out of sight
			if enemy.position.x + enemy.width < 0 then
				table.remove(subby.enemies, i)
				subby.score.passes = subby.score.passes + 1
			end
		end

		updateEnemyMovement()
		deleteEnemy()
	end

	from(subby.enemies):each(updateEnemy)
end

local updateSpawn = function(enemies)

	local spawnDue = function()
		if subby.spawnTimer.interval > 0 then subby.spawnTimer.interval = subby.spawnTimer.interval - subby._.dTime
		else
			subby.spawnTimer.interval = config.spawnTimer.interval
			if subby.spawnTimer.max > subby.spawnTimer.min then
				subby.spawnTimer.max = subby.spawnTimer.max - subby.spawnTimer.decrease
			end
		end
		if subby.spawnTimer.current > 0 then
			subby.spawnTimer.current = subby.spawnTimer.current - subby._.dTime
			return false
		end
		subby.spawnTimer.current = math.random(subby.spawnTimer.min, subby.spawnTimer.max)
		return true
	end

	if (spawnDue()) then
		local type = config.enemyTypes[math.random(1, #config.enemyTypes)]
		table.insert(subby.enemies, config[type]:prototype {
			position = {
				x = subby._.canvas.width,
				y = math.random(0, subby._.canvas.height - config.enemy.height)
			},
		})
	end
end

enemies = {}

function enemies:update()
	updateMovement(self)
	updateSpawn(self)
end

table.setreadonly(enemies)